package com.eugenegolobokin.models;

import com.eugenegolobokin.exceptions.MaximumNumberOfStudentsReached;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GroupTest {
    @Test
    public void testDistinctStudents() throws MaximumNumberOfStudentsReached {
        Group group = new Group("Test group");

        Student student = new Student("Test", "Tester", "2000-01-01");

        group.addStudent(student);

        assertEquals(1, group.getStudentList().size());

        group.addStudent(student);
        group.addStudent(student);

        assertEquals(1, group.getStudentList().size());

    }

    @Test
    public void testDistinctStudentsCornerCase() throws MaximumNumberOfStudentsReached {
        Group group = new Group("Test group");

        Student student1 = new Student("Test", "Tester", "2000-01-01");
        Student student2 = new Student("Test", "Tester", "2000-01-01");

        group.addStudent(student1);

        assertEquals(1, group.getStudentList().size());

        group.addStudent(student2);
        group.addStudent(student2);

        assertEquals(1, group.getStudentList().size());

    }

    @Test (expected = MaximumNumberOfStudentsReached.class)
    public void testStudentsLimit() throws MaximumNumberOfStudentsReached {
        Group group = new Group("Test group");
        Student student1 = new Student("Test1", "Tester", "2000-01-01");
        Student student2 = new Student("Test2", "Tester", "2000-01-01");
        Student student3 = new Student("Test3", "Tester", "2000-01-01");
        Student student4 = new Student("Test4", "Tester", "2000-01-01");
        Student student5 = new Student("Test5", "Tester", "2000-01-01");
        Student student6 = new Student("Test6", "Tester", "2000-01-01");

        group.addStudent(student1);
        group.addStudent(student2);
        group.addStudent(student3);
        group.addStudent(student4);
        group.addStudent(student5);
        group.addStudent(student6);

        assertEquals(5, group.getStudentList().size());
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testBreakTheSystem() throws MaximumNumberOfStudentsReached {
        Group group = new Group("Test group");
        for (int i = 0; i<5; i++) {
            Student s = new Student("test" + i, "Test", "2000-01-01");
            group.addStudent(s);
        }

        group.getStudentList().add(new Student("Hacker", "Hack", "1901-02-02"));

        assertEquals(5, group.getStudentList().size());
    }

}
