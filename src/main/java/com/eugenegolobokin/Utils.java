package com.eugenegolobokin;

import java.util.List;
import java.util.Random;

public class Utils {
    private static Random random = new Random();

    public static<T> T getRandom(List<T> collection){
        int r = random.nextInt(collection.size());
        return collection.get(r);
    }
}
