package com.eugenegolobokin.models;

import com.eugenegolobokin.exceptions.MaximumNumberOfStudentsReached;

import java.util.*;
import java.util.stream.Collectors;

public class Group {
    private static final int STUDENTS_LIMIT = 5;
    private final String name;
    private Trainer trainer;
    private Set<Student> studentList = new HashSet<>(25);


    public Group(String name) {
        this.name = name;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public void addStudent(Student student) throws MaximumNumberOfStudentsReached {
        if (studentList.size() == STUDENTS_LIMIT)
            throw new MaximumNumberOfStudentsReached();
        if (trainer != null) {
            student.setTrainer(trainer);
            trainer.addStudent(student);
        }
        this.studentList.add(student);
    }

    public Set<Student> getStudentList() {
        return Collections.unmodifiableSet(studentList);
    }

    public List<Student> getStudentListSorted() {
        return studentList.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public int getStudentCount() {
        return studentList.size();
    }

    public long getStudentCountWithoutJavaKnowledge(){
        return studentList
                .stream()
                .filter(s -> !s.hasPreviousJavaKnowledge())
                .count();
    }

    @Override
    public String toString() {
        return String.format(
                "<Group name: %s studentCount: %d studentsWithoutJavaKnowledge %d>",
                name, getStudentCount(), getStudentCountWithoutJavaKnowledge()
                );
    }

    public boolean cleanUp(){
        return studentList.removeIf(s -> s.getAge() <= 25);
    }
}
