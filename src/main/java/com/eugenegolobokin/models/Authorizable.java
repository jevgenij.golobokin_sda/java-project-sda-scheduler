package com.eugenegolobokin.models;

public interface Authorizable {
    boolean isAuthorized();
}
