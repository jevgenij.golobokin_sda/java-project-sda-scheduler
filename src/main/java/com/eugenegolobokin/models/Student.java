package com.eugenegolobokin.models;

import java.time.LocalDate;

public class Student extends Person {
    private Trainer trainer;
    private final boolean hasPreviousJavaKnowledge;

    public Student(String firstName, String lastName, String dateOfBirth) {
        super(firstName, lastName, dateOfBirth);
        this.hasPreviousJavaKnowledge = false;
    }

    public Student(String firstName, String lastName, String dateOfBirth, boolean hasPreviousJavaKnowledge) {
        super(firstName, lastName, dateOfBirth);
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    public boolean hasPreviousJavaKnowledge() {
        return hasPreviousJavaKnowledge;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
}
