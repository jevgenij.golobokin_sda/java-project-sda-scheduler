package com.eugenegolobokin;

import com.eugenegolobokin.exceptions.MaximumNumberOfStudentsReached;
import com.eugenegolobokin.models.Group;
import com.eugenegolobokin.models.Student;
import com.eugenegolobokin.models.Trainer;

import java.util.*;
import java.util.stream.Collectors;

public class Main {



    public static void main(String[] args) {


        // Generating Trainer list
        List<Trainer> trainerList = Arrays.asList(
                new Trainer("Jack", "Black", "2000-01-02"),
                new Trainer("John", "Snow", "1955-07-07"),
                new Trainer("Bob", "Sponge", "1988-12-12")
        );

        // Generating Group list
        List<Group> groupList = Arrays.asList(
                new Group("Java1"),
                new Group("Java2"),
                new Group("Java3"),
                new Group("Java4")
        );

        Random random = new Random();

        // Generating list of Students
        Stack<Student> studentStack = new Stack<>();
        for (int i = 0; i < 15; i++) {
//            String firstName = String.format("Student%d", i + 1);
//            String lastName = String.format("Student%c", (char) (65 + i));
//            String birthDate = String.format("199%d-01-01", random.nextInt(9));
//            studentStack.add(new Student(firstName, lastName, birthDate, random.nextBoolean()));


            List<String> firstNames = Arrays.asList("Jack", "John", "Ivan", "Bob", "Max", "Carlos", "Steve");
            List<String> lastNames = Arrays.asList("Bones", "Rocker", "Wick", "Pencil", "Brown", "Santana");
            String birthDate = String.format("199%d-04-01", random.nextInt(9));
            // LocalDate randomDate = LocalDate.of(1990 + rand.nextInt(9), 1, 1);

            studentStack.add(new Student(
                    Utils.getRandom(firstNames),
                    Utils.getRandom(lastNames),
                    birthDate,
                    random.nextBoolean()));
        }

        System.out.println(studentStack.toString());

        // Assigning trainers to groups
        for (Group group : groupList) {
            Trainer trainer = Utils.getRandom(trainerList);
            //Trainer trainer = trainerList.get(random.nextInt(trainerList.size() - 1));
            group.setTrainer(trainer);
        }

        // Assigning students to groups with exception if number of students > 5
        while (studentStack.size() > 0) {
            Student student = studentStack.pop();
            Group group = Utils.getRandom(groupList);
            //Group group = groupList.get(random.nextInt(groupList.size() - 1));
            try {
                group.addStudent(student);
            }
            catch (MaximumNumberOfStudentsReached mex) {
                studentStack.push(student);
            }

        }

        // Display all students in a group sorted alphabetically by lastName
        System.out.println("=========== sorted");
        for (Group group : groupList) {
            System.out.println("======" + group.getName());
            for (Student student : group.getStudentListSorted()) System.out.println(student);
        }


        // Display the group with the maximum number of students
        Group withMostStudents = groupList.stream()
                .sorted((o1, o2) -> {
                   return Integer.compare(o2.getStudentCount(), o1.getStudentCount());
                })
                .findFirst()
                .get();

        System.out.printf("%s group is with most students.\n", withMostStudents.getName());



        // Display all students younger than 25, from all groups
        Set<Student> youngStudents = groupList
                .stream()
                .map(g -> g.getStudentList())
                .flatMap(Collection::stream)
                .filter(student -> student.getAge() < 25)
                .collect(Collectors.toSet());

        System.out.println("Young students: ");
        for (Student student : youngStudents) System.out.println(student + " " + student.getAge());



        // Display all students grouped by trainer that teaches to them
        for (Trainer t : trainerList) {
            System.out.printf("Trainer: %s :", t.getFirstName());
            for (Student student : t.getStudents()) {
                System.out.println(student);
            }
        }

        // Display all students with previous java knowledge
        groupList
                .stream()
                .map(g -> g.getStudentList())
                .flatMap(Collection::stream)
                .filter(s -> s.hasPreviousJavaKnowledge())
                .forEach(s -> System.out.println(s));


        // Display the group with the highest number of students with no previous java knowledge
        groupList
                .stream()
                .sorted((g1,g2)-> {
                    long count1 = g1.getStudentCountWithoutJavaKnowledge();

                    long count2 = g2.getStudentCountWithoutJavaKnowledge();

                    return  Long.compare(count2, count1);
                })
                .findFirst()
                .ifPresent(g -> System.out.printf("%s has largest number of students without Java knowledge", g));


        // Remove all the students younger than 25 from all groups
        for (Group group :groupList) {
            group.cleanUp();
        }
        System.out.println("Students after cleanup:");
        for (Group group : groupList) {
            for (Student student : group.getStudentList()) {
                System.out.println(student + ", age: " + student.getAge());
            }
        }

    }
}
